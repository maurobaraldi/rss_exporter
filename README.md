# RSS Exporter - Export RSS feed to CSV in pure Python solution.

## Setup

The only setup needed is create a text file called **urllist.txt** with list of URLs (one per line) in same path than app file.

## Run

There is two way to run it, in a Docker container or in current environment.

### Docker

```
docker-compose up --build
```

or

```
make container-run
```

### Environment

```
python main.py urllist.txt
```

or

```
make run
```

## Use as a lib

If you want to use it as a lib to your project, just import it and use the features.

## ToDo List

- Use a config file and load settings from config file.

- Save log to file.

- Add tests.
