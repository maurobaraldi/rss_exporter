#!/usr/bin/env python

'''
RSS Exporter - Export RSS data to CSV file.

To run it as a script, pass the full path of url list file as argument.
'''
import logging
import sys
from csv import DictWriter
from time import time
from urllib.error import HTTPError, URLError
from urllib.parse import urlparse
from urllib.request import Request, urlopen
from xml.etree import ElementTree as ET


logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', encoding='utf-8', level=logging.DEBUG)

def request(url: str) -> str:
    '''Request (GET) to given URL.

    :param url: URL to request data.
    :type url: str
    :rtype: str
    :return: The result of request is 200 else empty string.  
    '''
    headers = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36'}
    request = Request(url=url, headers=headers)
    try:
        with urlopen(request) as r:
            return r.read().decode("utf8")
    except (HTTPError, URLError) as err:
        logging.warning(f"Check the URL {url}. Erro: {err.reason}")
        return ""

def parse(data: str, source: str) -> list:
    '''Parse data from XML to dict.

    :param data: Data to be parsed
    :type data: str
    :param source: URL of the source (just for info/log)
    :type source: str
    :return: A list with dicts of objects or a empty list
    '''
    result = []
    try:
        tree = ET.fromstring(text=data)
    except ET.ParseError:
        logging.warning(f'Source {source} not parsed. Check it consistence.')
        return []

    for item in tree.find('channel').findall('item'):
        # Adding only items that has decription (because this is the indexing field).
        if item.findall('description'):
            article = {
                # commenting the language, because many of sources does not inform 
                # this info, althought most of them are in english.
                #'language': tree.find('channel').find('language').text if tree.find('channel').find('language') else "",
                'title': item.find('title').text,
                'link': item.find('link').text,
                'pubdate': item.find('pubDate').text,
                'description': item.findall('description')[0].text,
            }
            result.append(article)
    return result

def save_to_csv(dataset: list, source: str, filename: str, mode: str = 'a') -> None:
    '''Save datase (list of dictionaries) to CSV file.

    :param dataset: List of dicts with articles.
    :type data: list
    :param source: URL of the source (just for info/log)
    :type source: str
    :param filename: Full path of file to store the articles.
    :type data: str
    :param mode: Mode of file handling ((w)rite or (a)ppend) defaults to a.
    :type data: str
    '''
    if len(dataset) < 1:
        logging.warning(f'Dataset {source} not exported because dataset was empty.')
        return None

    with open(file=filename, mode=mode, newline='') as csvfile:
            fieldnames = ['title', 'link', 'pubdate', 'description']
            writer = DictWriter(f=csvfile, fieldnames=fieldnames, delimiter=";")
            writer.writeheader()
            for row in dataset:
                writer.writerow(row)

def etl(urls_file: str) -> None:
    '''Perform a Extract-Transform-Load proccess given the lis of URLs.

    :param urls_file: Full path of file containing the URLs.
    :type data: str
    '''
    with open(urls_file) as fp:
        sources = [i.strip() for i in fp.readlines()]
    
    logging.info(f'Loading sources list file {urls_file} with {len(sources)} urls to be processed.')

    for source in sources:
        e = request(url=source)
        t = parse(data=e, source=source)
        l = save_to_csv(dataset=t, source=source, filename='cryptonews.csv', mode='a')

        logging.info(f'Successfully processed {len(t)} registries from {urlparse(source).netloc}')

if __name__ == '__main__':
    if len(sys.argv) > 1:
        start = time()
        etl(sys.argv[1])
        end = time()

        logging.info(f'Extracted, Transformed and Loaded all dataset in {end - start} seconds.')
