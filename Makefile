.PHONY: build run up down clean-pyc

# CI/CD
build: ## builds the application image
	docker-compose build exporter

run: ## creates the application container and run it (then, show logs)!
	python main.py urllist.txt

container-run: ## creates the application container and run it (then, show logs)!
	docker-compose up --build -d && docker-compose logs -f

clean-pyc: ## Remove compiled bytecode of source files
	@find . -name '*.pyc' -exec rm -f {} +
	@find . -name '*.pyo' -exec rm -f {} +
	@find . -name '*~' -exec rm -f {} +
	@find . -name '__pycache__' -exec rm -fr {} +
	@find . -name '*.pytest_cache' -exec rm -fr {} +